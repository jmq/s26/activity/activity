// - What directive is used by Node.js in loading the modules it needs?
// - require()
// - What Node.js module contains a method for server creation?
// - http
// - What is the method of the http object responsible for creating a server using Node.js?
// - createServer()
// - What method of the response object allows us to set status codes and content types?
// - writeHead()
// - Where will console.log() output its contents when run in Node.js?
// - terminal
// - What property of the request object contains the address's endpoint?
// - url

// 5. Import the http module using the required directive.
const http = require("http")
// 6. Create a variable port and assign it with the value of 3000.
const port = 3000
// 7. Create a server using the createServer method that will listen in to the port provided above.
const server = http.createServer((req, res) => {
	// Create a condition that when the login route is accessed, 
	// it will print a message to the user that they are in the login page.
	if(req.url == '/login') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('You are in the login page.')
	}
	// 11. Create a condition for any other routes that will return an error message.
	else {
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end('Page not found!')
	}
})

server.listen(port)
// 8. Console log in the terminal a message when the server is successfully running.
console.log(`Server now accessible at localhoses:${port}.`)


// 10. Access the login route to test if it’s working as intended.
// Result: working
// 12. Access any other route to test if it’s working as intended.
// Result: working
